<?php

namespace App\Controllers;

use Slim;
use Slim\Http\Request;
use Slim\Http\Response;
use Monolog;

class UserController
{
    /** @var Monolog\Logger $logger */
    protected $logger;

    /** @var PDO $db  */
    protected $db;

    public function __construct(Slim\Container $container) 
    {
        $this->logger = $container->logger;
        $this->db = $container->db;
    }

    public function get(Request $request, Response $response, $args): Response
    {
        $id = intval($args['id']);
        $this->logger->info("index '/user/{$id}' route");

        $sth = $this->db->prepare("SELECT name, email FROM users where id = :id");
        $sth->bindParam("id", $id);
        $sth->execute();
        $user = $sth->fetchObject();

        return $response->withJson($user);
    }
}