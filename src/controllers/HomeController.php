<?php

namespace App\Controllers;

use Slim;
use Slim\Http\Request;
use Slim\Http\Response;
use Monolog;

class HomeController
{
    /** @var Slim\Views\PhpRenderer $view  */
    protected $view;

    public function __construct(Slim\Container $container) 
    {
        $this->view = $container->view;
    }

    public function index(Request $request, Response $response, $args): Response
    {
        $response = $this->view->render($response, 'index.html');
        return $response;
    }
}