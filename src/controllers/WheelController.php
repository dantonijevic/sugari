<?php

namespace App\Controllers;

use Slim;
use Slim\Http\Request;
use Slim\Http\Response;
use Monolog;
use stdClass;

class WheelController
{
    private $treeIds = array(2, 3, 5, 6, 8, 9, 11, 12);
    private $premiumEntryTreeCost = 4;

    /** @var Monolog\Logger $logger */
    protected $logger;

    /** @var PDO $db  */
    protected $db;

    public function __construct(Slim\Container $container) 
    {
        $this->logger = $container->logger;
        $this->db = $container->db;
    }

    protected function getAllowedUserSpinCountFromDb(int $userId, string $period = null): int
    {
        $sth = $this->db->prepare(
            "SELECT count(*) 
            FROM user_wheel_spins 
            WHERE user_id = :user_id 
            AND period = :period 
            AND (
                spin_datetime IS NULL 
                OR spin_datetime = ''
                ) 
            ORDER BY id"
        );
        
        $period = $period ?: date("Y-m");
        $sth->bindParam("period", $period);
        $sth->bindParam("user_id", $userId);
        $sth->execute();
        
        return intval($sth->fetchColumn());
    }

    public function getAllowedUserSpinCount(Request $request, Response $response, $args): Response
    {
        $userId = intval($args['userId']);
        $this->logger->info("index '/getAllowedUserSpinCount/{$userId}' route");
        
        $allowedUserSpinCount = $this->getAllowedUserSpinCountFromDb($userId);
        
        return $response->withJson(array(
            "allowedUserSpinCount" => $allowedUserSpinCount,
        ));
    }

    protected function getUserSpinAndTreeCountsFromDb(int $userId, string $period = null): stdClass
    {
        $sth = $this->db->prepare(
            "SELECT count(ruws.id) AS remaining_spins, uwsl.max_tries, u.tree_count 
            FROM users u  
            LEFT JOIN user_wheel_spins ruws ON (
                u.id = ruws.user_id  
                AND ruws.period = :period 
                AND (ruws.spin_datetime IS NULL OR ruws.spin_datetime = '')
            )
            LEFT JOIN user_wheel_spin_limits uwsl ON (
                u.id = uwsl.user_id 
                AND uwsl.period = :period 
            )
            WHERE u.id = :user_id
            GROUP BY uwsl.max_tries, u.tree_count"
        );
        $period = $period ?: date("Y-m");
        $sth->bindParam("period", $period);
        $sth->bindParam("user_id", $userId);
        $sth->execute();
        $wheelSummary = $sth->fetchObject();
        
        return $wheelSummary;
    }

    protected function getPeriodPremiumRewardFromDb(string $period = null): array
    {
        $sth = $this->db->prepare(
            "SELECT name, quantity 
            FROM premium_rewards 
            WHERE period = :period"
        );
        $period = $period ?: date("Y-m");
        $sth->bindParam("period", $period);
        $sth->execute();
        $nextPeriodPremiumRewards = array_map(function ($reward) {
            $reward["quantity"] = intval($reward["quantity"]);
            return $reward;
        }, $sth->fetchAll());

        return $nextPeriodPremiumRewards;
    }

    public function getWheelSummary(Request $request, Response $response, $args): Response
    {
        $userId = intval($args['userId']);
        $this->logger->info("index '/getWheelSummary/{$userId}' route");
        
        $userSpinAndTreeCounts = $this->getUserSpinAndTreeCountsFromDb($userId);
    
        $nextPeriod = date("Y-m", strtotime('first day of +1 month'));
        $nextPeriodPremiumRewards = $this->getPeriodPremiumRewardFromDb($nextPeriod);
            
        return $response->withJson(array(
            "remainingSpins" => intval($userSpinAndTreeCounts->remaining_spins),
            "totalSpins" => intval($userSpinAndTreeCounts->max_tries),
            "treeCount" => intval($userSpinAndTreeCounts->tree_count),
            "nextPeriodPremiumRewards" => $nextPeriodPremiumRewards,
        ));
    }
    
    public function spinTheWheel(Request $request, Response $response, $args): Response
    {
        $userId = intval($args['userId']);
        $this->logger->info("index '/spinTheWheel/{$userId}' route");

        $sth = $this->db->prepare(
            "SELECT id, discount
            FROM user_wheel_spins 
            WHERE user_id = :user_id 
            AND period = :period
            AND (
                spin_datetime IS NULL 
                OR spin_datetime = ''
            ) 
            ORDER BY id
            LIMIT 1"
        );

        $period = date("Y-m");
        $sth->bindParam("period", $period);
        $sth->bindParam("user_id", $userId);
        $sth->execute();
        $completedSpin = $sth->fetchObject();

        if (!$completedSpin) {
            return $response->withStatus(400)
                ->withHeader('Content-Type', 'text/html')
                ->write('Žao nam je, ali nemate više pokušaja ovog meseca');
        }

        $sth = $this->db->prepare("UPDATE user_wheel_spins SET spin_datetime = now() WHERE id = :id");
        $sth->bindParam("id", $completedSpin->id);
        $sth->execute();

        if (in_array(intval($completedSpin->discount), $this->treeIds)) {
            $sth = $this->db->prepare("UPDATE users SET tree_count = tree_count + 1 WHERE id = :id");
            $sth->bindParam("id", $userId);
            $sth->execute();
        }

        return $response->withJson(array("rewardId" => $completedSpin->discount));
    }

    public function enterPremiumLottery(Request $request, Response $response, $args): Response
    {
        $userId = intval($args['userId']);
        $this->logger->info("index '/enterPremiumLottery/{$userId}' route");

        $sth = $this->db->prepare("SELECT tree_count FROM users where id = :id");
        $sth->bindParam("id", $userId);
        $sth->execute();
        $userTreeCount = $sth->fetchColumn();

        if (intval($userTreeCount) < $this->premiumEntryTreeCost) {
            return $response->withStatus(400)
                ->withHeader('Content-Type', 'text/html')
                ->write('Žao nam je, ali nemate dovoljno drveta za učestvovanje u nagradnoj igri');
        }

        $sth = $this->db->prepare("UPDATE users SET tree_count = tree_count - :premium_entry_tree_cost WHERE id = :id");
        $sth->bindParam("premium_entry_tree_cost", $this->premiumEntryTreeCost);
        $sth->bindParam("id", $userId);
        $sth->execute();

        $nextPeriod = date("Y-m", strtotime('first day of +1 month'));
        $sth = $this->db->prepare("INSERT INTO user_premium_entries (user_id, period) VALUES (:user_id, :period)");
        $sth->bindParam("period", $nextPeriod);
        $sth->bindParam("user_id", $userId);
        $sth->execute();

        return $response->withJson(array("message" => "Uspešno ste se prijavili za nagradnu igru"));
    }
}