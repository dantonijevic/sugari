<?php

// Routes
$app->get('/', App\Controllers\HomeController::class . ':index');
$app->get('/dashboard', App\Controllers\HomeController::class . ':index');

$app->get('/user/{id}', App\Controllers\UserController::class . ':get');

$app->get('/getAllowedUserSpinCount/{userId}', App\Controllers\WheelController::class . ':getAllowedUserSpinCount');

$app->get('/getWheelSummary/{userId}', App\Controllers\WheelController::class . ':getWheelSummary');

$app->get('/spinTheWheel/{userId}', App\Controllers\WheelController::class . ':spinTheWheel');

$app->get('/enterPremiumLottery/{userId}', App\Controllers\WheelController::class . ':enterPremiumLottery');
