import React from "react";
import {TweenMax} from "gsap/all";
import Winwheel from "../../../node_modules/winwheel/index.js";
import axios from "axios";
import withStyles from "material-ui/styles/withStyles";
import {blue, grey} from "material-ui/colors";
import _ from "lodash";
import {Done} from "@material-ui/icons";
import {
    Grid,
    Button,
    Tooltip,
    Snackbar,
    Dialog,
    DialogTitle,
    Typography,
    List,
    ListItem,
    ListItemText,
    ListItemAvatar,
    Avatar
} from "material-ui";
import Slide from "material-ui/transitions/Slide";
import classNames from "classnames";
import Confetti from "react-dom-confetti";

const styleSheet = {
    grid: {
        position: 'relative',
        paddingRight: '0 !important',
        flexGrow: 1,
        textAlign: 'center'
    },
    button: {
        backgroundColor: blue[400]
    },
    text: {
        fontWeight: 'bold',
        marginTop: '-30px'
    },
    treeRow: {
        float: 'left'
    },
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 60,
        height: 60,
    }
};

class Wheel extends React.Component {
    treeIds = [2, 3, 5, 6, 8, 9, 11, 12];
    spinDuration = 5;
    premiumLotteryEntryTreeCost = 4;
    startupMessages = [
        'Uštedite novac plaćajući svoj Telenor racun elektronski bez provizije.',
        'Koristeći eRačun umesto papirnog računa čuvate životnu sredinu.',
        'Uštedite vreme plaćajući svoj Telenor racun elektronski bez odlaska u banku.',
    ];

    state = {
        userId: 4,
        counts: null,
        nextPeriodPremiumRewards: null,
        message: '',
        openPremiumDialog: false,
        fireConfetti: false
    };

    componentDidMount() {
        this.updateSummaryData();
        this.updateCanvas();

        let time = 0;
        _.each(this.startupMessages, message => {
            setTimeout(() => {
                this.showMessage(message)
            }, time);
            time += 11000;
        });
    };

    updateSummaryData = () => {
        const wheel = this;
        axios.get(`http://sugari.tn/getWheelSummary/${this.state.userId}`)
            .then(function (response) {
                wheel.setState({
                    nextPeriodPremiumRewards: response.data.nextPeriodPremiumRewards,
                    counts: {
                        remainingSpins: response.data.remainingSpins,
                        totalSpins: response.data.totalSpins,
                        treeCount: response.data.treeCount
                    }
                });
            });
    };

    updateCanvas(selectedSegment = null) {
        var myWheel = new Winwheel({
            'canvasId': 'myCanvas',
            'numSegments': 12,
            'outerRadius': 190,
            'textMargin': 20,
            'textFontFamily': 'Roboto',
            'lineWidth': 0,
            'innerRadius': 30,
            'rotationAngle': -15,
            //'drawText': true,
            //'drawMode': 'segmentImage',
            'drawMode': 'image',
            'segments': [
                {'fillStyle': '#e5e100', 'text': '50 RSD'},
                {'fillStyle': '#ffffff', 'text': ''},
                {'fillStyle': '#ffffff', 'text': ''},
                {'fillStyle': '#00c110', 'text': '150 RSD'},
                {'fillStyle': '#ffffff', 'text': ''},
                {'fillStyle': '#ffffff', 'text': ''},
                {'fillStyle': '#616bf4', 'text': '250 RSD'},
                {'fillStyle': '#ffffff', 'text': ''},
                {'fillStyle': '#ffffff', 'text': ''},
                {'fillStyle': '#d80202', 'text': '350 RSD'},
                {'fillStyle': '#ffffff', 'text': ''},
                {'fillStyle': '#ffffff', 'text': ''}
            ],
            'animation': {
                'type': 'spinToStop',
                'duration': this.spinDuration,
                'spins': 8
            }
        });

        var loadedImg = new Image();

// Create callback to execute once the image has finished loading.
        loadedImg.onload = function () {
            myWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
            myWheel.draw();                    // Also call draw function to render the wheel.
        };

// Set the image source, once complete this will trigger the onLoad callback (above).
        loadedImg.src = "images/tceo2.jpg";

        if (selectedSegment) {
            // Get random angle inside specified segment of the wheel.
            var stopAt = myWheel.getRandomForSegment(selectedSegment);

            // Important thing is to set the stopAngle of the animation before stating the spin.
            myWheel.animation.stopAngle = stopAt;

            // Start the spin animation here.
            myWheel.startAnimation();
        }
    }

    spinTheWheel = () => {
        const {state, spinDuration} = this;

        if (!state.counts) {
            this.showMessage('Podaci se još nisu učitali');
            return false;
        }

        if (state.counts.remainingSpins <= 0) {
            this.showMessage('Žao nam je, ali nemate više pokušaja ovog meseca');
            return false;
        }

        const wheel = this;
        axios.get(`http://sugari.tn/spinTheWheel/${state.userId}`)
            .then(function (response) {
                const {rewardId} = response.data;

                if (response <= 0) {
                    wheel.showMessage('Došlo je do greške');
                    return false;
                }

                const isTree = _.includes(wheel.treeIds, parseInt(rewardId, 10));

                wheel.updateCanvas(rewardId);
                setTimeout(
                    () => {
                        const newCounts = {...state.counts};
                        newCounts.remainingSpins--;
                        if (isTree) {
                            newCounts.treeCount++;
                        }

                        wheel.setState({counts: newCounts});

                        const reward = isTree ? 'drvo' : 'popust';
                        wheel.showMessage(`Čestitke! Osvojili ste ${reward}!`);
                        if (!isTree) {
                            wheel.setState({fireConfetti: true});
                            wheel.setState({fireConfetti: false});
                        }
                    },
                    spinDuration * 1000
                );
            })
            .catch(function (error) {
                wheel.showMessage(error);
            });
    };

    getSpinCounts = () => {
        return this.state.counts
            ? (<p>{this.state.counts.remainingSpins} / {this.state.counts.totalSpins}</p>)
            : (<p>Učitavanje broja pokušaja</p>);
    };

    getTreeCount = () => {
        return this.state.counts
            ? (<h1 style={{fontWeight: 'bold'}}>{this.state.counts.treeCount} x</h1>)
            : (<p>Učitacanje broja drveća</p>);
    };

    getPremiumRewardItems = (nextPeriodPremiumRewards) => {
        return _.isArray(nextPeriodPremiumRewards)
        ? nextPeriodPremiumRewards.map(premiumReward => (
            <ListItem button key={premiumReward.name}>
                <ListItemAvatar>
                    <Avatar
                        alt={premiumReward.name}
                        src={`./images/${premiumReward.name}.jpg`}
                        className={classNames(this.props.classes.avatar, this.props.classes.bigAvatar)}
                    />
                </ListItemAvatar>
                <ListItemText primary={premiumReward.name} />
            </ListItem>
        ))
        : <ListItem button key={0}>
            <ListItemText primary={"Učitavanje podataka o premium nagradama"} />
        </ListItem>;
    };

    handleApplyForPremium = () => {
        const wheel = this;

        if (!wheel.state.counts) {
            wheel.showMessage('Podaci o Vašem broju drveta se nisu učitali');
            return false
        }

        if (wheel.premiumLotteryEntryTreeCost > wheel.state.counts.treeCount) {
            wheel.showMessage('Žao nam je ali nemate potreban broj drveta za učestvovanje u nagradnoj igri');
            return false
        }

        axios.get(`http://sugari.tn/enterPremiumLottery/${wheel.state.userId}`)
            .then(function (response) {
                wheel.showMessage(response.data.message);
                const newCounts = {...wheel.state.counts};
                newCounts.treeCount = newCounts.treeCount - wheel.premiumLotteryEntryTreeCost;
                wheel.setState({
                    counts: newCounts,
                    openPremiumDialog: false

                });
            })
            .catch(function (error) {
                wheel.showMessage(error);
            });
    };

    showMessage = (message) => {
        this.setState({message: message});
    };

    render() {
        const {classes} = this.props;
        return (
            <div>
                <Snackbar
                    style={{top: '70px', zIndex: '9999999'}}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    open={this.state.message.length > 0}
                    onClose={() => this.setState({message: ''})}
                    autoHideDuration={10000}
                    transition={Slide}
                    SnackbarContentProps={{'aria-describedby': 'message-id'}}
                    message={<span style={{fontSize: '1.2em'}} id="message-id">{this.state.message}</span>}
                />
                <div className={classes.grid}>
                    <div>
                        <img
                            src="images/telenorlogo.png"
                            height="100"
                            width="100"
                            style={{marginBottom: '-70px', transform: 'rotate(30deg)', marginLeft: '-2px'}}>
                        </img>
                    </div>
                    <canvas id="myCanvas" ref="canvas" width={450} height={450}/>

                    <p className={classes.text}>Broj pokušaja: {this.getSpinCounts()} </p>
                    <Confetti active={ this.state.fireConfetti } config={{angle: 45, startVelocity: 50}} />
                    <Button
                        variant="raised"
                        className={classes.button}
                        onClick={(event) => this.spinTheWheel()}
                    >
                        OKRENI
                    </Button>
                </div>
                <div>
                    <Grid container xs={4}>
                        {this.getTreeCount()}
                        <Tooltip
                            id="tooltip-top-start"
                            title="Klikni da kupiš šansu za učešće u premium nagradi"
                            placement="top-start"
                        >
                            <img src="images/tree.png"
                                 height="50" width="50"
                                 style={{marginTop: '30px', marginLeft: '10px'}}
                                 onClick={ () => { this.setState({openPremiumDialog: true}) } }
                            >
                            </img>
                        </Tooltip>
                        <Dialog
                            open={this.state.openPremiumDialog}
                            onClose={ () => { this.setState({openPremiumDialog: false}) } }
                            aria-labelledby="simple-dialog-title"
                        >
                            <DialogTitle id="simple-dialog-title">Premium nagradna igra</DialogTitle>
                            <Typography variant="subheading" id="simple-modal-description" style={{padding: 10}}>
                                Za 4 drveta mozete da učestvujete u nagradnoj igri.
                                <br />Možete da osvojite jednu od sledećih nagrada:
                            </Typography>
                            <List>
                                { this.getPremiumRewardItems(this.state.nextPeriodPremiumRewards) }
                                <ListItem button onClick={() => this.handleApplyForPremium()}>
                                    <Avatar>
                                        <Done />
                                    </Avatar>
                                    <ListItemText primary="Prijavi se" />
                                </ListItem>
                            </List>
                        </Dialog>
                    </Grid>
                </div>
            </div>
        );
    };
}

export default withStyles(styleSheet)(Wheel);
