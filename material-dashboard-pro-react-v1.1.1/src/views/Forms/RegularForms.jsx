import React from "react";
import withStyles from "material-ui/styles/withStyles";
import MailOutline from "@material-ui/icons/MailOutline";
import GridContainer from "components/Grid/GridContainer.jsx";
import ItemGrid from "components/Grid/ItemGrid.jsx";
import IconCard from "components/Cards/IconCard.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";
import Person from "@material-ui/icons/Person";
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";
import IconButton from "components/CustomButtons/IconButton.jsx";
import Table from "components/Table/Table.jsx";


class RegularForms extends React.Component {
    constructor(props) {

        super(props);

        this.state = {
            checked: [24, 22],
            selectedValue: null,
            selectedEnabled: "b",
            distributionData: [],
            budget: 0,
            recommendedBudget: 400000,
            maxBudget: 410000,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeEnabled = this.handleChangeEnabled.bind(this);
    }


    handleChange(event) {


        this.setState({selectedValue: event.target.value});
    }

    handleChangeBudget(event) {
        console.log(event.target.value);
        this.setState({budget: event.target.value});
    }

    calculateAwardsDistribution() {

        let qty = 1
        let realBudget = 0;
        let budget = this.state.budget;
        let award4 = 350;
        let award3 = 250;
        let award2 = 150;
        let award1 = 50;


        while (true) {

            realBudget = award4 * qty + award3 * qty ** 2 + award2 * qty ** 3 + award1 * qty ** 4;

            if (realBudget >= budget) {

                qty = qty - 0.1

                realBudget = award4 * Math.round(qty) + award3 * Math.round(qty ** 2) + award2 * Math.round(qty ** 3) + award1 * Math.round(qty ** 4);

                this.setState({
                    distributionData: [
                        ["1", "50 RSD", Math.round(qty ** 4), Math.round(qty ** 4) * 50 + " RSD"],
                        ["2", "150 RSD", Math.round(qty ** 3), Math.round(qty ** 3) * 150 + " RSD"],
                        ["3", "250 RSD", Math.round(qty ** 2), Math.round(qty ** 2) * 250 + " RSD"],
                        ["4", "350 RSD", Math.round(qty), Math.round(qty) * 50 + " RSD"],
                    ],
                    recommendedBudget: realBudget
                });

                return;

            } else qty = qty + 0.1;

        }


    }

    getAwardsDistribution() {
        return this.state.distributionData;
    }

    handleChangeEnabled(event) {
        this.setState({selectedEnabled: event.target.value});
    }

    handleToggle(value) {
        const {checked} = this.state;
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        this.setState({
            checked: newChecked
        });
    }

    render() {
        const {classes} = this.props;
        const fillButtons = [
            {color: "info", icon: Person},
            {color: "success", icon: Edit},
            {color: "danger", icon: Close}
        ].map((prop, key) => {
            return (
                <Button color={prop.color} customClass={classes.actionButton} key={key}>
                    <prop.icon className={classes.icon}/>
                </Button>
            );
        });
        const simpleButtons = [
            {color: "infoNoBackground", icon: Person},
            {color: "successNoBackground", icon: Edit},
            {color: "dangerNoBackground", icon: Close}
        ].map((prop, key) => {
            return (
                <Button color={prop.color} customClass={classes.actionButton} key={key}>
                    <prop.icon className={classes.icon}/>
                </Button>
            );
        });
        const roundButtons = [
            {color: "info", icon: Person},
            {color: "success", icon: Edit},
            {color: "danger", icon: Close}
        ].map((prop, key) => {
            return (
                <IconButton color={prop.color} customClass={classes.actionButton + " " + classes.actionButtonRound}
                            key={key}>
                    <prop.icon className={classes.icon}/>
                </IconButton>
            );
        });

        return (
            <GridContainer>
                <ItemGrid >
                    <IconCard
                        icon={MailOutline}
                        iconColor="rose"
                        title="Total eBill customers: 360569"
                        content={
              <form>
                <CustomInput
                  labelText="Budget"
                  id="budget"
                  formControlProps={{
                    fullWidth: true,
                    disabled: true
                  }}
                  inputProps={{
                    type: "text",
                    value: "456.696 RSD"
                  }}
                />


                <CustomInput
                  labelText="Fund for awards"
                  id="fundFor Awards"
                  onChange ={(event) => { this.handleChangeBudget(event.target.value)}}
                  formControlProps={{
                    fullWidth: true,
                  }}
                  inputProps={{
                    onChange: event => this.handleChangeBudget(event),
                    type: "text"
                  }}
                  helpText= {"Recommended fund: " + this.state.recommendedBudget}
                />

                <Button onClick={(event) => {this.calculateAwardsDistribution()}} color="rose">Optimize</Button>
              </form>
            }
                    />
                </ItemGrid>


                <ItemGrid xs={12} sm={12} md={6}>

                    <IconCard
                        icon={MailOutline}
                        iconColor="rose"
                        title="Distribution"
                        content={
                         <form>

              <Table
                tableHead={[
                  "#",
                  "Discount",
                  "Qty",
                  "Sum",

                ]}
                tableData= {this.getAwardsDistribution()}
                customCellClasses={[
                  classes.center,
                  classes.right,
                  classes.right
                ]}
                customClassesForCells={[0, 4, 5]}
                customHeadCellClasses={[
                  classes.center,
                  classes.right,
                  classes.right
                ]}
                customHeadClassesForCells={[0, 4, 5]}
              />
                <Button onClick={(event) => {this.calculateAwardsDistribution()}} color="rose">Distribute</Button>
              </form>

            }
                    />

                </ItemGrid>

            </GridContainer>
        );
    }
}

export default withStyles(regularFormsStyle)(RegularForms);
