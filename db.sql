-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2018 at 11:20 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hackaton-2018-05`
--
CREATE DATABASE IF NOT EXISTS `hackaton-2018-05` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `hackaton-2018-05`;

-- --------------------------------------------------------

--
-- Table structure for table `premium_rewards`
--

DROP TABLE IF EXISTS `premium_rewards`;
CREATE TABLE `premium_rewards` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `name` varchar(50) NOT NULL,
  `period` varchar(15) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tree_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_premium_entries`
--

DROP TABLE IF EXISTS `user_premium_entries`;
CREATE TABLE `user_premium_entries` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `period` varchar(15) NOT NULL,
  `premum_reward_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_wheel_spins`
--

DROP TABLE IF EXISTS `user_wheel_spins`;
CREATE TABLE `user_wheel_spins` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `period` varchar(15) NOT NULL,
  `spin_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_wheel_spin_limits`
--

DROP TABLE IF EXISTS `user_wheel_spin_limits`;
CREATE TABLE `user_wheel_spin_limits` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `period` varchar(15) NOT NULL,
  `max_tries` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wheel_rewards`
--

DROP TABLE IF EXISTS `wheel_rewards`;
CREATE TABLE `wheel_rewards` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `discount` int(11) NOT NULL,
  `period` varchar(15) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `premium_rewards`
--
ALTER TABLE `premium_rewards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_premium_entries`
--
ALTER TABLE `user_premium_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wheel_rewards`
--
ALTER TABLE `wheel_rewards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_wheel_spins`
--
ALTER TABLE `user_wheel_spins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_wheel_spin_limits`
--
ALTER TABLE `user_wheel_spin_limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`name`, `email`) VALUES
('Petar Nešić', 'Petar.Nesic@telenor.rs'),
('Mirko Dučić', 'Mirko.Ducic@telenor.rs'),
('Dušan Antonijević', 'Dusan.Antonijevic@telenor.rs'),
('Dimitrije Jančić', 'Dimitrije.Jancic@telenor.rs');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
